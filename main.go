package main

import (
	"bitbucket.org/task-aviasales/services/address"
	"bitbucket.org/task-aviasales/services/private/cache"
	"bitbucket.org/task-aviasales/services/private/server"

	"bitbucket.org/task-aviasales/gen/clients/suggest_api/client"
	"bitbucket.org/task-aviasales/gen/server/operations"

	"github.com/pkg/errors"
	"go.uber.org/zap"
)

func main() {
	s := server.NewServer(&server.Config{})
	defer s.Srv.Shutdown()

	addr := address.NewAddress(&address.Config{
		Log:     s.Log,
		Suggest: client.Default.Operations,
		Cache: cache.NewCache(map[string]string{
			s.Conf.RedisHost: s.Conf.RedisPort,
		}),
	})

	s.Api.SuggestHandler = operations.SuggestHandlerFunc(addr.Suggest)

	// serve API
	if err := s.Srv.Serve(); err != nil {
		s.Log.Fatal("server failed", zap.Error(errors.WithStack(err)))
	}
}
