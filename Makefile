install_tools:
	brew tap go-swagger/go-swagger
	brew install go-swagger

.PHONY: gen
gen:
	mkdir -p ./gen ./gen/clients/suggest_api ./gen/mocks/address
	swagger generate server --exclude-main -A server -f ./swagger.yaml -t ./gen -s ./server -m ./server/models
	swagger generate client -f clients_spec/suggest_api/swagger.yaml -A suggest_api -t ./gen/clients/suggest_api -m ./models
	minimock -i services/address.SuggestApi -o gen/mocks/address -s "_minimock.go"
	minimock -i services/address.Cache -o gen/mocks/address -s "_minimock.go"

dependencies_up:
	docker-compose up -d --build

dependencies_down:
	docker-compose down

test:
	@go test -race -cover ./services/...