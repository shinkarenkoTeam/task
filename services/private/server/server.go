package server

import (
	"bitbucket.org/task-aviasales/gen/server"
	"bitbucket.org/task-aviasales/gen/server/operations"

	"github.com/go-openapi/loads"
	"github.com/kelseyhightower/envconfig"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Config struct {
	LoggerLevel string `envconfig:"LOGGER_LEVEL" default:"info"`
	Port        int    `envconfig:"SERVER_PORT" default:"8080"`
	DevMode     bool   `envconfig:"DEVELOPMENT_MODE" default:"false"`
	RedisHost   string `envconfig:"REDIS_HOST" default:"localhost"`
	RedisPort   string `envconfig:"REDIS_PORT" default:":6379"`
}

type Server struct {
	Srv *server.Server
	Api *operations.ServerAPI
	Log *zap.Logger

	Conf *Config
}

func NewServer(c *Config) *Server {
	err := envconfig.Process("", c)
	if err != nil {
		panic(err.Error())
	}

	level := zap.NewAtomicLevel()
	err = level.UnmarshalText([]byte(c.LoggerLevel))
	if err != nil {
		panic(err)
	}

	logconf := zap.Config{
		Level:       level,
		Encoding:    getLoggerEncoding(c),
		Development: c.DevMode,
		OutputPaths: []string{"stdout"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:     "msg",
			LevelKey:       "level",
			TimeKey:        "timestamp",
			EncodeLevel:    getEncoderLevel(c),
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.FullCallerEncoder,
		},
	}

	log, err := logconf.Build()
	if err != nil {
		panic(err)
	}

	swaggerSpec, err := loads.Analyzed(server.SwaggerJSON, "")
	if err != nil {
		panic(err.Error())
	}

	api := operations.NewServerAPI(swaggerSpec)

	srv := server.NewServer(api)
	srv.Port = c.Port

	return &Server{
		Srv:  srv,
		Api:  api,
		Log:  log,
		Conf: c,
	}
}

func getLoggerEncoding(c *Config) string {
	if c.DevMode {
		return "console"
	}
	return "json"
}

func getEncoderLevel(c *Config) zapcore.LevelEncoder {
	if c.DevMode {
		return zapcore.LowercaseColorLevelEncoder
	}
	return zapcore.LowercaseLevelEncoder
}
