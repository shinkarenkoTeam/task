package private

import (
	"net/http"
	"time"
)

var DefaultHTTPClient = &http.Client{
	Timeout: 1 * time.Second,
}
