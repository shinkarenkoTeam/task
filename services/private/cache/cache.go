package cache

import (
	"encoding/json"

	"github.com/go-redis/cache"
	"github.com/go-redis/redis"
)

func NewCache(addrs map[string]string) *cache.Codec {
	ring := redis.NewRing(&redis.RingOptions{
		Addrs: addrs,
	})

	codec := &cache.Codec{
		Redis: ring,

		Marshal: func(v interface{}) ([]byte, error) {
			return json.Marshal(v)
		},
		Unmarshal: func(b []byte, v interface{}) error {
			return json.Unmarshal(b, v)
		},
	}
	return codec
}
