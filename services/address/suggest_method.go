package address

import (
	"fmt"
	"time"

	"bitbucket.org/task-aviasales/gen/clients/suggest_api/client/operations"
	"bitbucket.org/task-aviasales/gen/clients/suggest_api/models"
	"bitbucket.org/task-aviasales/services/private"

	servModels "bitbucket.org/task-aviasales/gen/server/models"
	servOperations "bitbucket.org/task-aviasales/gen/server/operations"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-redis/cache"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

var (
	ttl, _            = time.ParseDuration("1h")
	errInternalServer = servOperations.NewSuggestInternalServerError().WithPayload(&servModels.Error{
		Message: "something was wrong",
	})
)

func (a *Address) Suggest(args servOperations.SuggestParams) middleware.Responder {
	cacheResp := make([]*servModels.SuggestResponse, 0, 5)
	err := a.cache.Get(args.HTTPRequest.URL.RawQuery, &cacheResp)

	switch err {
	case nil:
		return servOperations.NewSuggestOK().WithPayload(cacheResp)
	case cache.ErrCacheMiss:
	default:
		a.log.Error("/address/suggest cache get error", zap.Error(errors.WithStack(err)))
	}

	r, err := a.suggest.PlacesJSON(&operations.PlacesJSONParams{
		Locale:     args.Locale,
		Term:       args.Term,
		Types:      args.Types,
		Context:    args.HTTPRequest.Context(),
		HTTPClient: private.DefaultHTTPClient,
	})

	if err != nil {
		a.log.Error("error", zap.Error(errors.WithStack(err)))
		return errInternalServer
	}

	resp := a.convertSuggestResponse(r.Payload)
	err = a.cache.Set(&cache.Item{
		Key:        args.HTTPRequest.URL.RawQuery,
		Object:     &resp,
		Expiration: ttl,
	})
	if err != nil {
		a.log.Error("/address/suggest cache set error", zap.Error(errors.WithStack(err)))
	}

	return servOperations.NewSuggestOK().WithPayload(resp)
}

func (a *Address) convertSuggestResponse(args []*models.PlacesResponse) []*servModels.SuggestResponse {
	res := make([]*servModels.SuggestResponse, 0, len(args))
	for _, obj := range args {

		sub := ""
		switch obj.Type {
		case models.PlacesResponseTypeCity:
			sub = obj.CountryName
		case models.PlacesResponseTypeAirport:
			sub = obj.CityName
		default:
			a.log.Error("wrong response suggest-api",
				zap.Error(errors.WithStack(fmt.Errorf("invalid type %s ", obj.Type))))
			continue
		}
		res = append(res, &servModels.SuggestResponse{
			Slug:     obj.Code,
			Subtitle: sub,
			Title:    obj.Name,
		})
	}
	return res
}
