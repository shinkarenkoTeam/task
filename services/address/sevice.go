package address

import (
	"bitbucket.org/task-aviasales/gen/clients/suggest_api/client/operations"

	"github.com/go-redis/cache"
	"go.uber.org/zap"
)

type SuggestApi interface {
	PlacesJSON(params *operations.PlacesJSONParams) (*operations.PlacesJSONOK, error)
}

type Cache interface {
	Get(key string, object interface{}) error
	Set(item *cache.Item) error
}

type Address struct {
	log     *zap.Logger
	suggest SuggestApi
	cache   Cache
}

type Config struct {
	Log     *zap.Logger
	Suggest SuggestApi
	Cache   Cache
}

func NewAddress(c *Config) *Address {
	return &Address{
		log:     c.Log,
		suggest: c.Suggest,
		cache:   c.Cache,
	}
}
