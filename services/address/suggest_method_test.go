package address

import (
	"net/http"
	"net/url"
	"reflect"
	"testing"

	"bitbucket.org/task-aviasales/gen/clients/suggest_api/client/operations"
	"bitbucket.org/task-aviasales/gen/clients/suggest_api/models"
	"bitbucket.org/task-aviasales/gen/mocks/address"
	servModels "bitbucket.org/task-aviasales/gen/server/models"
	servOperations "bitbucket.org/task-aviasales/gen/server/operations"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-redis/cache"
	"go.uber.org/zap"
)

func TestAddress_Suggest(t *testing.T) {
	cliResponse := &operations.PlacesJSONOK{
		Payload: []*models.PlacesResponse{
			{
				CityName:    "Москва",
				Code:        "DME",
				CountryName: "Россия",
				Name:        "Домодедово",
				Type:        "airport",
			},
			{
				CityName:    "Москва",
				Code:        "MOW",
				CountryName: "Россия",
				Name:        "Москва",
				Type:        "city",
			},
			{
				Type: "error",
			},
		},
	}
	apiResponse := servOperations.NewSuggestOK().WithPayload([]*servModels.SuggestResponse{
		{
			Slug:     "DME",
			Subtitle: "Москва",
			Title:    "Домодедово",
		},
		{
			Slug:     "MOW",
			Subtitle: "Россия",
			Title:    "Москва",
		},
	})

	params := servOperations.SuggestParams{
		HTTPRequest: &http.Request{
			URL: &url.URL{},
		},
	}

	type fields struct {
		log     *zap.Logger
		suggest SuggestApi
		cache   Cache
	}
	type args struct {
		args servOperations.SuggestParams
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   middleware.Responder
	}{
		{
			name: "OkWithoutCache",
			fields: fields{
				log:     zap.NewNop(),
				suggest: address.NewSuggestApiMock(t).PlacesJSONMock.Return(cliResponse, nil),
				cache: address.NewCacheMock(t).GetMock.Return(cache.ErrCacheMiss).
					SetMock.Return(nil),
			},
			args: args{
				args: params,
			},
			want: apiResponse,
		},
		{
			name: "OkWithCache",
			fields: fields{
				log:   zap.NewNop(),
				cache: address.NewCacheMock(t).GetMock.Return(nil),
			},
			args: args{
				args: params,
			},
			want: servOperations.NewSuggestOK().WithPayload([]*servModels.SuggestResponse{}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &Address{
				log:     tt.fields.log,
				suggest: tt.fields.suggest,
				cache:   tt.fields.cache,
			}
			if got := a.Suggest(tt.args.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Address.Suggest() = %v, want %v", got, tt.want)
			}
		})
	}
}
