package address

/*
DO NOT EDIT!
This code was generated automatically using github.com/gojuno/minimock v1.9
The original interface "SuggestApi" can be found in bitbucket.org/task-aviasales/services/address
*/
import (
	"sync/atomic"
	"time"

	operations "bitbucket.org/task-aviasales/gen/clients/suggest_api/client/operations"
	"github.com/gojuno/minimock"

	testify_assert "github.com/stretchr/testify/assert"
)

//SuggestApiMock implements bitbucket.org/task-aviasales/services/address.SuggestApi
type SuggestApiMock struct {
	t minimock.Tester

	PlacesJSONFunc       func(p *operations.PlacesJSONParams) (r *operations.PlacesJSONOK, r1 error)
	PlacesJSONCounter    uint64
	PlacesJSONPreCounter uint64
	PlacesJSONMock       mSuggestApiMockPlacesJSON
}

//NewSuggestApiMock returns a mock for bitbucket.org/task-aviasales/services/address.SuggestApi
func NewSuggestApiMock(t minimock.Tester) *SuggestApiMock {
	m := &SuggestApiMock{t: t}

	if controller, ok := t.(minimock.MockController); ok {
		controller.RegisterMocker(m)
	}

	m.PlacesJSONMock = mSuggestApiMockPlacesJSON{mock: m}

	return m
}

type mSuggestApiMockPlacesJSON struct {
	mock              *SuggestApiMock
	mainExpectation   *SuggestApiMockPlacesJSONExpectation
	expectationSeries []*SuggestApiMockPlacesJSONExpectation
}

// SuggestApiMockPlacesJSONExpectation specifies expectation struct of the SuggestApi.PlacesJSON
type SuggestApiMockPlacesJSONExpectation struct {
	input  *SuggestApiMockPlacesJSONInput
	result *SuggestApiMockPlacesJSONResult
}

// SuggestApiMockPlacesJSONInput represents input parameters of the SuggestApi.PlacesJSON
type SuggestApiMockPlacesJSONInput struct {
	p *operations.PlacesJSONParams
}

// SuggestApiMockPlacesJSONResult represents results of the SuggestApi.PlacesJSON
type SuggestApiMockPlacesJSONResult struct {
	r  *operations.PlacesJSONOK
	r1 error
}

//Expect specifies that invocation of SuggestApi.PlacesJSON is expected from 1 to Infinity times
func (m *mSuggestApiMockPlacesJSON) Expect(p *operations.PlacesJSONParams) *mSuggestApiMockPlacesJSON {
	m.mock.PlacesJSONFunc = nil
	m.expectationSeries = nil

	if m.mainExpectation == nil {
		m.mainExpectation = &SuggestApiMockPlacesJSONExpectation{}
	}
	m.mainExpectation.input = &SuggestApiMockPlacesJSONInput{p}
	return m
}

//Return specifies results of invocation of SuggestApi.PlacesJSON
func (m *mSuggestApiMockPlacesJSON) Return(r *operations.PlacesJSONOK, r1 error) *SuggestApiMock {
	m.mock.PlacesJSONFunc = nil
	m.expectationSeries = nil

	if m.mainExpectation == nil {
		m.mainExpectation = &SuggestApiMockPlacesJSONExpectation{}
	}
	m.mainExpectation.result = &SuggestApiMockPlacesJSONResult{r, r1}
	return m.mock
}

//ExpectOnce specifies that invocation of SuggestApi.PlacesJSON is expected once
func (m *mSuggestApiMockPlacesJSON) ExpectOnce(p *operations.PlacesJSONParams) *SuggestApiMockPlacesJSONExpectation {
	m.mock.PlacesJSONFunc = nil
	m.mainExpectation = nil

	expectation := &SuggestApiMockPlacesJSONExpectation{}
	expectation.input = &SuggestApiMockPlacesJSONInput{p}
	m.expectationSeries = append(m.expectationSeries, expectation)
	return expectation
}

//Return sets up return arguments of expectation struct for SuggestApi.PlacesJSON
func (e *SuggestApiMockPlacesJSONExpectation) Return(r *operations.PlacesJSONOK, r1 error) {
	e.result = &SuggestApiMockPlacesJSONResult{r, r1}
}

//Set uses given function f as a mock of SuggestApi.PlacesJSON method
func (m *mSuggestApiMockPlacesJSON) Set(f func(p *operations.PlacesJSONParams) (r *operations.PlacesJSONOK, r1 error)) *SuggestApiMock {
	m.mainExpectation = nil
	m.expectationSeries = nil

	m.mock.PlacesJSONFunc = f
	return m.mock
}

//PlacesJSON implements bitbucket.org/task-aviasales/services/address.SuggestApi interface
func (m *SuggestApiMock) PlacesJSON(p *operations.PlacesJSONParams) (r *operations.PlacesJSONOK, r1 error) {
	counter := atomic.AddUint64(&m.PlacesJSONPreCounter, 1)
	defer atomic.AddUint64(&m.PlacesJSONCounter, 1)

	if len(m.PlacesJSONMock.expectationSeries) > 0 {
		if counter > uint64(len(m.PlacesJSONMock.expectationSeries)) {
			m.t.Fatalf("Unexpected call to SuggestApiMock.PlacesJSON. %v", p)
			return
		}

		input := m.PlacesJSONMock.expectationSeries[counter-1].input
		testify_assert.Equal(m.t, *input, SuggestApiMockPlacesJSONInput{p}, "SuggestApi.PlacesJSON got unexpected parameters")

		result := m.PlacesJSONMock.expectationSeries[counter-1].result
		if result == nil {
			m.t.Fatal("No results are set for the SuggestApiMock.PlacesJSON")
			return
		}

		r = result.r
		r1 = result.r1

		return
	}

	if m.PlacesJSONMock.mainExpectation != nil {

		input := m.PlacesJSONMock.mainExpectation.input
		if input != nil {
			testify_assert.Equal(m.t, *input, SuggestApiMockPlacesJSONInput{p}, "SuggestApi.PlacesJSON got unexpected parameters")
		}

		result := m.PlacesJSONMock.mainExpectation.result
		if result == nil {
			m.t.Fatal("No results are set for the SuggestApiMock.PlacesJSON")
		}

		r = result.r
		r1 = result.r1

		return
	}

	if m.PlacesJSONFunc == nil {
		m.t.Fatalf("Unexpected call to SuggestApiMock.PlacesJSON. %v", p)
		return
	}

	return m.PlacesJSONFunc(p)
}

//PlacesJSONMinimockCounter returns a count of SuggestApiMock.PlacesJSONFunc invocations
func (m *SuggestApiMock) PlacesJSONMinimockCounter() uint64 {
	return atomic.LoadUint64(&m.PlacesJSONCounter)
}

//PlacesJSONMinimockPreCounter returns the value of SuggestApiMock.PlacesJSON invocations
func (m *SuggestApiMock) PlacesJSONMinimockPreCounter() uint64 {
	return atomic.LoadUint64(&m.PlacesJSONPreCounter)
}

//PlacesJSONFinished returns true if mock invocations count is ok
func (m *SuggestApiMock) PlacesJSONFinished() bool {
	// if expectation series were set then invocations count should be equal to expectations count
	if len(m.PlacesJSONMock.expectationSeries) > 0 {
		return atomic.LoadUint64(&m.PlacesJSONCounter) == uint64(len(m.PlacesJSONMock.expectationSeries))
	}

	// if main expectation was set then invocations count should be greater than zero
	if m.PlacesJSONMock.mainExpectation != nil {
		return atomic.LoadUint64(&m.PlacesJSONCounter) > 0
	}

	// if func was set then invocations count should be greater than zero
	if m.PlacesJSONFunc != nil {
		return atomic.LoadUint64(&m.PlacesJSONCounter) > 0
	}

	return true
}

//ValidateCallCounters checks that all mocked methods of the interface have been called at least once
//Deprecated: please use MinimockFinish method or use Finish method of minimock.Controller
func (m *SuggestApiMock) ValidateCallCounters() {

	if !m.PlacesJSONFinished() {
		m.t.Fatal("Expected call to SuggestApiMock.PlacesJSON")
	}

}

//CheckMocksCalled checks that all mocked methods of the interface have been called at least once
//Deprecated: please use MinimockFinish method or use Finish method of minimock.Controller
func (m *SuggestApiMock) CheckMocksCalled() {
	m.Finish()
}

//Finish checks that all mocked methods of the interface have been called at least once
//Deprecated: please use MinimockFinish or use Finish method of minimock.Controller
func (m *SuggestApiMock) Finish() {
	m.MinimockFinish()
}

//MinimockFinish checks that all mocked methods of the interface have been called at least once
func (m *SuggestApiMock) MinimockFinish() {

	if !m.PlacesJSONFinished() {
		m.t.Fatal("Expected call to SuggestApiMock.PlacesJSON")
	}

}

//Wait waits for all mocked methods to be called at least once
//Deprecated: please use MinimockWait or use Wait method of minimock.Controller
func (m *SuggestApiMock) Wait(timeout time.Duration) {
	m.MinimockWait(timeout)
}

//MinimockWait waits for all mocked methods to be called at least once
//this method is called by minimock.Controller
func (m *SuggestApiMock) MinimockWait(timeout time.Duration) {
	timeoutCh := time.After(timeout)
	for {
		ok := true
		ok = ok && m.PlacesJSONFinished()

		if ok {
			return
		}

		select {
		case <-timeoutCh:

			if !m.PlacesJSONFinished() {
				m.t.Error("Expected call to SuggestApiMock.PlacesJSON")
			}

			m.t.Fatalf("Some mocks were not called on time: %s", timeout)
			return
		default:
			time.Sleep(time.Millisecond)
		}
	}
}

//AllMocksCalled returns true if all mocked methods were called before the execution of AllMocksCalled,
//it can be used with assert/require, i.e. assert.True(mock.AllMocksCalled())
func (m *SuggestApiMock) AllMocksCalled() bool {

	if !m.PlacesJSONFinished() {
		return false
	}

	return true
}
