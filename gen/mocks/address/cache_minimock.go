package address

/*
DO NOT EDIT!
This code was generated automatically using github.com/gojuno/minimock v1.9
The original interface "Cache" can be found in bitbucket.org/task-aviasales/services/address
*/
import (
	"sync/atomic"
	"time"

	cache "github.com/go-redis/cache"
	"github.com/gojuno/minimock"

	testify_assert "github.com/stretchr/testify/assert"
)

//CacheMock implements bitbucket.org/task-aviasales/services/address.Cache
type CacheMock struct {
	t minimock.Tester

	GetFunc       func(p string, p1 interface{}) (r error)
	GetCounter    uint64
	GetPreCounter uint64
	GetMock       mCacheMockGet

	SetFunc       func(p *cache.Item) (r error)
	SetCounter    uint64
	SetPreCounter uint64
	SetMock       mCacheMockSet
}

//NewCacheMock returns a mock for bitbucket.org/task-aviasales/services/address.Cache
func NewCacheMock(t minimock.Tester) *CacheMock {
	m := &CacheMock{t: t}

	if controller, ok := t.(minimock.MockController); ok {
		controller.RegisterMocker(m)
	}

	m.GetMock = mCacheMockGet{mock: m}
	m.SetMock = mCacheMockSet{mock: m}

	return m
}

type mCacheMockGet struct {
	mock              *CacheMock
	mainExpectation   *CacheMockGetExpectation
	expectationSeries []*CacheMockGetExpectation
}

// CacheMockGetExpectation specifies expectation struct of the Cache.Get
type CacheMockGetExpectation struct {
	input  *CacheMockGetInput
	result *CacheMockGetResult
}

// CacheMockGetInput represents input parameters of the Cache.Get
type CacheMockGetInput struct {
	p  string
	p1 interface{}
}

// CacheMockGetResult represents results of the Cache.Get
type CacheMockGetResult struct {
	r error
}

//Expect specifies that invocation of Cache.Get is expected from 1 to Infinity times
func (m *mCacheMockGet) Expect(p string, p1 interface{}) *mCacheMockGet {
	m.mock.GetFunc = nil
	m.expectationSeries = nil

	if m.mainExpectation == nil {
		m.mainExpectation = &CacheMockGetExpectation{}
	}
	m.mainExpectation.input = &CacheMockGetInput{p, p1}
	return m
}

//Return specifies results of invocation of Cache.Get
func (m *mCacheMockGet) Return(r error) *CacheMock {
	m.mock.GetFunc = nil
	m.expectationSeries = nil

	if m.mainExpectation == nil {
		m.mainExpectation = &CacheMockGetExpectation{}
	}
	m.mainExpectation.result = &CacheMockGetResult{r}
	return m.mock
}

//ExpectOnce specifies that invocation of Cache.Get is expected once
func (m *mCacheMockGet) ExpectOnce(p string, p1 interface{}) *CacheMockGetExpectation {
	m.mock.GetFunc = nil
	m.mainExpectation = nil

	expectation := &CacheMockGetExpectation{}
	expectation.input = &CacheMockGetInput{p, p1}
	m.expectationSeries = append(m.expectationSeries, expectation)
	return expectation
}

//Return sets up return arguments of expectation struct for Cache.Get
func (e *CacheMockGetExpectation) Return(r error) {
	e.result = &CacheMockGetResult{r}
}

//Set uses given function f as a mock of Cache.Get method
func (m *mCacheMockGet) Set(f func(p string, p1 interface{}) (r error)) *CacheMock {
	m.mainExpectation = nil
	m.expectationSeries = nil

	m.mock.GetFunc = f
	return m.mock
}

//Get implements bitbucket.org/task-aviasales/services/address.Cache interface
func (m *CacheMock) Get(p string, p1 interface{}) (r error) {
	counter := atomic.AddUint64(&m.GetPreCounter, 1)
	defer atomic.AddUint64(&m.GetCounter, 1)

	if len(m.GetMock.expectationSeries) > 0 {
		if counter > uint64(len(m.GetMock.expectationSeries)) {
			m.t.Fatalf("Unexpected call to CacheMock.Get. %v %v", p, p1)
			return
		}

		input := m.GetMock.expectationSeries[counter-1].input
		testify_assert.Equal(m.t, *input, CacheMockGetInput{p, p1}, "Cache.Get got unexpected parameters")

		result := m.GetMock.expectationSeries[counter-1].result
		if result == nil {
			m.t.Fatal("No results are set for the CacheMock.Get")
			return
		}

		r = result.r

		return
	}

	if m.GetMock.mainExpectation != nil {

		input := m.GetMock.mainExpectation.input
		if input != nil {
			testify_assert.Equal(m.t, *input, CacheMockGetInput{p, p1}, "Cache.Get got unexpected parameters")
		}

		result := m.GetMock.mainExpectation.result
		if result == nil {
			m.t.Fatal("No results are set for the CacheMock.Get")
		}

		r = result.r

		return
	}

	if m.GetFunc == nil {
		m.t.Fatalf("Unexpected call to CacheMock.Get. %v %v", p, p1)
		return
	}

	return m.GetFunc(p, p1)
}

//GetMinimockCounter returns a count of CacheMock.GetFunc invocations
func (m *CacheMock) GetMinimockCounter() uint64 {
	return atomic.LoadUint64(&m.GetCounter)
}

//GetMinimockPreCounter returns the value of CacheMock.Get invocations
func (m *CacheMock) GetMinimockPreCounter() uint64 {
	return atomic.LoadUint64(&m.GetPreCounter)
}

//GetFinished returns true if mock invocations count is ok
func (m *CacheMock) GetFinished() bool {
	// if expectation series were set then invocations count should be equal to expectations count
	if len(m.GetMock.expectationSeries) > 0 {
		return atomic.LoadUint64(&m.GetCounter) == uint64(len(m.GetMock.expectationSeries))
	}

	// if main expectation was set then invocations count should be greater than zero
	if m.GetMock.mainExpectation != nil {
		return atomic.LoadUint64(&m.GetCounter) > 0
	}

	// if func was set then invocations count should be greater than zero
	if m.GetFunc != nil {
		return atomic.LoadUint64(&m.GetCounter) > 0
	}

	return true
}

type mCacheMockSet struct {
	mock              *CacheMock
	mainExpectation   *CacheMockSetExpectation
	expectationSeries []*CacheMockSetExpectation
}

// CacheMockSetExpectation specifies expectation struct of the Cache.Set
type CacheMockSetExpectation struct {
	input  *CacheMockSetInput
	result *CacheMockSetResult
}

// CacheMockSetInput represents input parameters of the Cache.Set
type CacheMockSetInput struct {
	p *cache.Item
}

// CacheMockSetResult represents results of the Cache.Set
type CacheMockSetResult struct {
	r error
}

//Expect specifies that invocation of Cache.Set is expected from 1 to Infinity times
func (m *mCacheMockSet) Expect(p *cache.Item) *mCacheMockSet {
	m.mock.SetFunc = nil
	m.expectationSeries = nil

	if m.mainExpectation == nil {
		m.mainExpectation = &CacheMockSetExpectation{}
	}
	m.mainExpectation.input = &CacheMockSetInput{p}
	return m
}

//Return specifies results of invocation of Cache.Set
func (m *mCacheMockSet) Return(r error) *CacheMock {
	m.mock.SetFunc = nil
	m.expectationSeries = nil

	if m.mainExpectation == nil {
		m.mainExpectation = &CacheMockSetExpectation{}
	}
	m.mainExpectation.result = &CacheMockSetResult{r}
	return m.mock
}

//ExpectOnce specifies that invocation of Cache.Set is expected once
func (m *mCacheMockSet) ExpectOnce(p *cache.Item) *CacheMockSetExpectation {
	m.mock.SetFunc = nil
	m.mainExpectation = nil

	expectation := &CacheMockSetExpectation{}
	expectation.input = &CacheMockSetInput{p}
	m.expectationSeries = append(m.expectationSeries, expectation)
	return expectation
}

//Return sets up return arguments of expectation struct for Cache.Set
func (e *CacheMockSetExpectation) Return(r error) {
	e.result = &CacheMockSetResult{r}
}

//Set uses given function f as a mock of Cache.Set method
func (m *mCacheMockSet) Set(f func(p *cache.Item) (r error)) *CacheMock {
	m.mainExpectation = nil
	m.expectationSeries = nil

	m.mock.SetFunc = f
	return m.mock
}

//Set implements bitbucket.org/task-aviasales/services/address.Cache interface
func (m *CacheMock) Set(p *cache.Item) (r error) {
	counter := atomic.AddUint64(&m.SetPreCounter, 1)
	defer atomic.AddUint64(&m.SetCounter, 1)

	if len(m.SetMock.expectationSeries) > 0 {
		if counter > uint64(len(m.SetMock.expectationSeries)) {
			m.t.Fatalf("Unexpected call to CacheMock.Set. %v", p)
			return
		}

		input := m.SetMock.expectationSeries[counter-1].input
		testify_assert.Equal(m.t, *input, CacheMockSetInput{p}, "Cache.Set got unexpected parameters")

		result := m.SetMock.expectationSeries[counter-1].result
		if result == nil {
			m.t.Fatal("No results are set for the CacheMock.Set")
			return
		}

		r = result.r

		return
	}

	if m.SetMock.mainExpectation != nil {

		input := m.SetMock.mainExpectation.input
		if input != nil {
			testify_assert.Equal(m.t, *input, CacheMockSetInput{p}, "Cache.Set got unexpected parameters")
		}

		result := m.SetMock.mainExpectation.result
		if result == nil {
			m.t.Fatal("No results are set for the CacheMock.Set")
		}

		r = result.r

		return
	}

	if m.SetFunc == nil {
		m.t.Fatalf("Unexpected call to CacheMock.Set. %v", p)
		return
	}

	return m.SetFunc(p)
}

//SetMinimockCounter returns a count of CacheMock.SetFunc invocations
func (m *CacheMock) SetMinimockCounter() uint64 {
	return atomic.LoadUint64(&m.SetCounter)
}

//SetMinimockPreCounter returns the value of CacheMock.Set invocations
func (m *CacheMock) SetMinimockPreCounter() uint64 {
	return atomic.LoadUint64(&m.SetPreCounter)
}

//SetFinished returns true if mock invocations count is ok
func (m *CacheMock) SetFinished() bool {
	// if expectation series were set then invocations count should be equal to expectations count
	if len(m.SetMock.expectationSeries) > 0 {
		return atomic.LoadUint64(&m.SetCounter) == uint64(len(m.SetMock.expectationSeries))
	}

	// if main expectation was set then invocations count should be greater than zero
	if m.SetMock.mainExpectation != nil {
		return atomic.LoadUint64(&m.SetCounter) > 0
	}

	// if func was set then invocations count should be greater than zero
	if m.SetFunc != nil {
		return atomic.LoadUint64(&m.SetCounter) > 0
	}

	return true
}

//ValidateCallCounters checks that all mocked methods of the interface have been called at least once
//Deprecated: please use MinimockFinish method or use Finish method of minimock.Controller
func (m *CacheMock) ValidateCallCounters() {

	if !m.GetFinished() {
		m.t.Fatal("Expected call to CacheMock.Get")
	}

	if !m.SetFinished() {
		m.t.Fatal("Expected call to CacheMock.Set")
	}

}

//CheckMocksCalled checks that all mocked methods of the interface have been called at least once
//Deprecated: please use MinimockFinish method or use Finish method of minimock.Controller
func (m *CacheMock) CheckMocksCalled() {
	m.Finish()
}

//Finish checks that all mocked methods of the interface have been called at least once
//Deprecated: please use MinimockFinish or use Finish method of minimock.Controller
func (m *CacheMock) Finish() {
	m.MinimockFinish()
}

//MinimockFinish checks that all mocked methods of the interface have been called at least once
func (m *CacheMock) MinimockFinish() {

	if !m.GetFinished() {
		m.t.Fatal("Expected call to CacheMock.Get")
	}

	if !m.SetFinished() {
		m.t.Fatal("Expected call to CacheMock.Set")
	}

}

//Wait waits for all mocked methods to be called at least once
//Deprecated: please use MinimockWait or use Wait method of minimock.Controller
func (m *CacheMock) Wait(timeout time.Duration) {
	m.MinimockWait(timeout)
}

//MinimockWait waits for all mocked methods to be called at least once
//this method is called by minimock.Controller
func (m *CacheMock) MinimockWait(timeout time.Duration) {
	timeoutCh := time.After(timeout)
	for {
		ok := true
		ok = ok && m.GetFinished()
		ok = ok && m.SetFinished()

		if ok {
			return
		}

		select {
		case <-timeoutCh:

			if !m.GetFinished() {
				m.t.Error("Expected call to CacheMock.Get")
			}

			if !m.SetFinished() {
				m.t.Error("Expected call to CacheMock.Set")
			}

			m.t.Fatalf("Some mocks were not called on time: %s", timeout)
			return
		default:
			time.Sleep(time.Millisecond)
		}
	}
}

//AllMocksCalled returns true if all mocked methods were called before the execution of AllMocksCalled,
//it can be used with assert/require, i.e. assert.True(mock.AllMocksCalled())
func (m *CacheMock) AllMocksCalled() bool {

	if !m.GetFinished() {
		return false
	}

	if !m.SetFinished() {
		return false
	}

	return true
}
